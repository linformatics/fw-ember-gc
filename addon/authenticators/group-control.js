import Base from 'ember-simple-auth/authenticators/base';
import service from 'ember-service/inject';

/**
 * This authenticator is used by Ember-Simple-Auth to authenticate
 * a user on the client.
 *
 * Example Usage:
 *
 * ```javascript
 * import Ember from 'ember';
 *
 * export default Ember.Object.extend({
 *     session: Ember.inject.service(),
 *
 *     login(username, password) {
 *         let session = this.get('session');
 *
 *         session.authenticate('authenticator:group-control', username, password);
 *     }
 * });
 * ```
 *
 * To extend in an application:
 *
 * ```javascript
 * import GCAuthenticator from 'fw-ember/authenticators/group-control';
 *
 * export default GCAuthenticator.extend({
 *     // ... your custom code here
 * });
 * ```
 *
 * For more example on using this in an application or extending it,
 * visit Ember Simple Auth's [website](http://ember-simple-auth.com/).
 *
 * @class GroupControlAuthenticator
 * @extends EmberSimpleAuth.BaseAuthenticator
 * @module fw-ember-gc
 */
export default Base.extend({

    /**
     * Injected Config service
     *
     * @property config
     * @type ConfigService
     */
    config: service(),

    /**
     * Injected Current User service
     *
     * @property currentUser
     * @type CurrentUserService
     */
    currentUser: service(),

    /**
     * Injected Ajax service
     *
     * @property ajax
     * @type AjaxService
     */
    ajax: service(),

    /**
     * Injected Notifications service
     *
     * @property notifications
     * @type NotificationsService
     */
    notifications: service(),

    /**
     * Overrides Ember-Simple-Auth's `restore` method.
     *
     * Restores user data on page reload. If the session has expired, automatically
     * logout the user.
     *
     * @method restore
     * @return {Promise} Promise that resolves on successful restoration, otherwise rejects
     */
    restore() {
        return this.get('ajax').request(this.get('config').formUrl('session', {program: this.get('config.programId'), app: 'gc'}))
            .then((data) => {
                this.get('currentUser').setProperties(data);
            });
    },

    /**
     * Authenticates a user given a username and password.
     *
     * @method authenticate
     * @param  {String} user Username to attempt login with
     * @param  {String} pass User-provided password to attempt login with
     * @return {Promise}     Promise that resolves upon successful login, otherwise rejects
     */
    authenticate(user, pass) {
        let program = this.get('config.programId');
        let data = JSON.stringify({
                user,
                pass,
                program
            });

        return this.get('ajax').post(this.get('config').formUrl('session', {app: 'gc'}), {
            data,
            contentType: 'application/json'
        }).then((data) => {
            if (data.isSecure === false) {
                this.get('currentUser').changePassword(true);
            }

            this.get('currentUser').setProperties(data);
        });
    },

    /**
     * Invalidates a user's session. Called on logout.
     *
     * @method invalidate
     * @return {Promise}
     */
    invalidate() {
        return this.get('ajax').del(this.get('config').formUrl('session', {app: 'gc'}));
    }
});
