import RSVP from 'rsvp';
import computed from 'ember-computed';
import LiquidTether from 'liquid-tether/components/liquid-tether';
import layout from 'fw-ember/templates/components/fw-fullscreen-modal';

const {Promise} = RSVP;

/**
 * Component that handles rendering of modals. To use this you must define
 * a component that extends the BaseModal component
 * and call this component with the following usage:
 *
 * ```handlebars
 * {{fw-fullscreen-modal "modal-name" size="md" close=(action 'closeModal') model=model}}
 * ```
 *
 * Note: Once this component appears in the DOM it will be rendered and shown,
 * so for most usage you should wrap it in an `{{if}}` helper and render it conditionalls
 *
 * ```handlebars
 * {{#if shouldRenderModal}}
 *     {{fw-fullscreen-modal "modal-name" size="md" ...}}
 * {{/if}}
 * ```
 *
 * When shouldRenderModal is `true`, the modal will appear, otherwise it will stay hidden.
 *
 * _**Note**: the component name of the modal is passed as the first parameter to the component.
 * When "`modal-name`" is passed, the component will be looked for in `app/components/modals/modalName.js`_
 *
 *
 * @class FullScreenModal
 * @extends LiquidTetherComponent
 * @module fw-ember
 */
const FullScreenModalComponent = LiquidTether.extend({
    layout,

    // LiquidTether properties. Shouldn't need to be overidden.
    to: 'fullscreen-modal',
    target: 'document.body',
    targetModifier: 'visible',
    targetAttachment: 'top center',
    attachment: 'top center',
    tetherClass: 'fullscreen-modal',
    overlayClass: 'fullscreen-modal-background',

    /**
     * Path to the currently rendered modal component.
     *
     * @protected
     * @property modalPath
     * @type {String}
     */
    modalPath: 'unknown',

    /**
     * Size of the modal. The three available are 'sm', 'md', and 'lg'.
     *
     * @property size
     * @type {String}
     */
    size: 'md',

    /**
     * Whether or not the modal should be able to be closed by clicking on the
     * background around it. Set this to be false if you do not want people to
     * be able to click out of the modal without prompting them first.
     *
     * @property closeOnClick
     * @type {Boolean}
     */
    closeOnClick: true,

    /**
     * Initializes the component and loads the modal path.
     *
     * @private
     * @method init
     */
    init() {
        this._super(...arguments);
        this.set('modalPath', `modals/${this.get('modal')}`);
    },

    /**
     * @private
     * @property sizeClass
     * @type {String}
     */
    sizeClass: computed('size', function () {
        return `fullscreen-modal-${this.get('size')}`;
    }),

    actions: {
        close() {
            if (this.attrs.close) {
                return this.attrs.close();
            }

            return new Promise((resolve) => {
                resolve();
            });
        },

        confirm() {
            if (this.attrs.confirm) {
                return this.attrs.confirm();
            }

            return new Promise((resolve) => {
                resolve();
            });
        },

        clickOverlay() {
            if (this.get('closeOnClick')) {
                this.send('close');
            }
        }
    }
});

FullScreenModalComponent.reopenClass({
    positionalParams: ['modal']
});

export default FullScreenModalComponent;
