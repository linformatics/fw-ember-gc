import Component from 'ember-component';
import {InvokeActionMixin} from 'ember-invoke-action';
import ValidatedInputMixin from 'fw-ember/mixins/validated-input';
import layout from 'fw-ember/templates/components/fw-validated-input-container';

export default Component.extend(InvokeActionMixin, ValidatedInputMixin, {
    layout,

    classNames: 'form-group',
    classNameBindings: 'state'
});
