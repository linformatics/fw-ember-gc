import Component from 'ember-component';
import computed from 'ember-computed';
import observer from 'ember-metal/observer';
import {later, cancel} from 'ember-runloop';
import layout from 'fw-ember/templates/components/fw-spin-button';
import {InvokeActionMixin} from 'ember-invoke-action';

/**
 * Borrowed from Ghost's 'gh-spin-button' component
 *
 * Button that spins to show something is happening.
 *
 * @class SpinButton
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend(InvokeActionMixin, {
    layout,
    tagName: 'button',

    /**
     * If you are rendering the promise button as an inline component and not
     * in block form, set this to whatever text you want the button to display.
     *
     * @property buttonText
     * @type {String}
     * @default ''
     */
    buttonText: '',

    /**
     * If you want a button-icon to be displayed, put any of the font-awesome icons
     * as a string here. _Note: you must include the 'fa-' before it. For example,
     * to display the check icon, set this to 'fa-check'_
     *
     * @property buttonIcon
     * @type {String}
     * @default ''
     */
    buttonIcon: '',

    /**
     * Sets the icon to be shown when the promise is resolving. See
     * [here](http://fontawesome.io/examples/#animated) for a list
     * of possible icons.
     *
     * @property spinIcon
     * @type {String}
     * @default 'fa-refresh'
     */
    spinIcon: 'fa-refresh',

    /**
     * Variable used to specify whether the button should be spinning or not.
     *
     * @property submitting
     * @type {Boolean}
     */
    submitting: false,

    /**
     * Private instance variable holding whether or not the button is currently
     * spinning.
     *
     * @private
     * @property showSpinner
     * @type {Boolean}
     */
    showSpinner: false,

    /**
     * Private variable that holds the timer event that manages the
     * switch back to not spinning after a `timeout` period.
     *
     * @private
     * @property showSpinnerTimeout
     * @type {Event}
     */
    showSpinnerTimeout: null,

    /**
     * Specifies whether or not the button should retain its width
     * when the spinner is activated.
     *
     * @property autoWidth
     * @type {Boolean}
     */
    autoWidth: true,

    /**
     * If true, the button will be a fixed width regardless of whether or
     * not the spinner is activated. (Used with {{#crossLink PromiseButton}}{{/crossLink}})
     *
     * @property fixWidth
     * @type {Boolean}
     */
    fixWidth: false,

    /**
     * Boolean property that disables the button when set to true.
     *
     * @property disableWhen
     * @type {Boolean}
     */
    disableWhen: false,

    /**
     * Sets the timeout of the button. If this is set, the button will
     * continue to spin for the time specified even after submitting is set to false.
     * This is useful if you wish to show that something happened, even if it happens
     * quickly.
     *
     * @property timeout
     * @type {Number}
     */
    timeout: 1000,

    // Disable button when button is spinning
    attributeBindings: ['disabled', 'type', 'tabindex'],

    disabled: computed('disableWhen', 'showSpinner', function () {
        return this.get('disableWhen') || (this.get('showSpinner') === true);
    }),

    click() {
        this.invokeAction('action');
        return false;
    },

    toggleSpinner: observer('submitting', function () {
        let submitting = this.get('submitting');
        let timeout = this.get('showSpinnerTimeout');

        if (submitting) {
            this.set('showSpinner', true);

            if (this.get('timeout')) {
                this.set('showSpinnerTimeout', later(this, function () {
                    if (!this.get('submitting')) {
                        this.set('showSpinner', false);
                    }
                    this.set('showSpinnerTimeout', null);
                }, this.get('timeout')));
            }
        } else if (!submitting && timeout === null) {
            this.set('showSpinner', false);
        }
    }),

    setSize: observer('showSpinner', function () {
        if ((this.get('showSpinner') || this.get('fixWidth')) && this.get('autoWidth')) {
            this.$().width(this.$().width());
            this.$().height(this.$().height());
        } else {
            this.$().width('');
            this.$().height('');
        }
    }),

    willDestroy() {
        this._super(...arguments);
        cancel(this.get('showSpinnerTimeout'));
    }
});
