import TextField from 'ember-components/text-field';

/**
 * Basically an extension of Ember's native `{{input}}` component,
 * but applies some Bootstrap styling. See Ember `{{input}}`
 * [documentation](https://guides.emberjs.com/v2.5.0/templates/input-helpers/)
 * for more information.
 *
 * @class FWInput
 * @extends Ember.TextField
 * @module fw-ember
 */
export default TextField.extend({
    classNames: 'form-control'
});
