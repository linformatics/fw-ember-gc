import Component from 'ember-component';
import computed, {alias} from 'ember-computed';
import service from 'ember-service/inject';
import layout from 'fw-ember/templates/components/fw-notification';

/**
 * Used with the {{#crossLink "Notifications"}}{{/crossLink}} component.
 *
 * @class Notification
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend({
    layout,
    tagName: 'article',
    classNames: ['alert', 'alert-dismissable'],
    classNameBindings: ['colorClass', 'dismissable:fw-alert-passive'],

    colorClass: computed('message.type', function () {
        return `alert-${this.get('message.type')}`;
    }),

    dismissable: alias('message.dismissable'),

    message: null,

    notifications: service(),

    didInsertElement() {
        this._super(...arguments);

        this.$().on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', (event) => {
            if (event.originalEvent.animationName === 'fade-out') {
                this.get('notifications').closeNotification(this.get('message'));
            }
        });
    },

    willDestroyElement() {
        this._super(...arguments);
        this.$().off('animationend webkitAnimationEnd oanimationend MSAnimationEnd');
    },

    actions: {
        closeNotification() {
            this.get('notifications').closeNotification(this.get('message'));
        }
    }
});
