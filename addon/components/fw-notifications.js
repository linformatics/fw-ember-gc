import Component from 'ember-component';
import service from 'ember-service/inject';
import {filter} from 'ember-computed';
import layout from 'fw-ember/templates/components/fw-notifications';

/**
 * Used for notifications. Can also be placed anywhere in the applcation, given
 * a provided "outlet" property, and notifications with that outlet will appear inside it.
 *
 * Example:
 * ```handlebars
 * {{fw-notifications outlet="global"}}
 * ```
 *
 * In the example, notifications with the "global" outlet specified will be rendered
 * inside this component using the Notification component. See the {{#crossLink NotificationsService}}{{/crossLink}}
 * documentation for more information.
 *
 * @class Notifications
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend({
    layout,
    tagName: 'aside',

    /**
     * The outlet to look for notifications in.
     *
     * @property outlet
     * @type {String}
     */
    outlet: 'default',

    classNames: 'alert-container',
    classNameBindings: 'outlet',

    notifications: service(),

    notificationsToShow: filter('notifications.content', function (item) {
        return item.outlet === this.get('outlet');
    })
});
