import Component from 'ember-component';
import PopoverMixin from 'fw-ember/mixins/popover';
import {InvokeActionMixin} from 'ember-invoke-action';

/**
 * Button that comes with the popover mixin
 *
 * @class PopoverButton
 * @extends Ember.Component
 * @uses PopoverMixin
 * @uses InvokeActionMixin
 * @module fw-ember
 */
export default Component.extend(PopoverMixin, InvokeActionMixin, {
    tagName: 'button',

    /**
     * Either a string action name or a closure action to call when the
     * button is clicked.
     *
     * @property action
     */

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */

    /**
     * Title of the popover
     * @property title
     */

    /**
     * Content of the popover
     * @property content
     */

    /**
     * Trigger of the popover (hover, click, or focus)
     * @property popoverTrigger
     */

    /**
     * Captures the click of a button and calls an optional "action" property
     *
     * @method click
     */
    click() {
        this.invokeAction('action');
    }
});
