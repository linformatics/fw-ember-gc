import Component from 'ember-component';
import TooltipMixin from 'fw-ember/mixins/tooltip';

/**
 * Icon that comes with the popover mixin
 *
 * @class TooltipIcon
 * @extends Ember.Component
 * @uses TooltipMixin
 * @module fw-ember
 */
export default Component.extend(TooltipMixin, {
    /**
     * Font-Awesome icon name
     * @property icon
     */
    icon: 'fa-info',

    tagName: 'i',
    classNames: ['fa'],
    classNameBindings: ['icon']

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */

    /**
     * text of the popover
     * @property text
     */
});
