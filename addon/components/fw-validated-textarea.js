import Component from 'ember-component';
import {scheduleOnce} from 'ember-runloop';
import ValidatedInputMixin from 'fw-ember/mixins/validated-input';
import {InvokeActionMixin} from 'ember-invoke-action';
import layout from 'fw-ember/templates/components/fw-validated-textarea';

export default Component.extend(InvokeActionMixin, ValidatedInputMixin, {
    layout,
    classNames: 'form-group',
    classNameBindings: 'state',

    inputId: '',
    name: '',
    placeholder: '',
    label: '',
    helpText: '',
    disabled: false,

    didInsertElement() {
        this._super(...arguments);

        scheduleOnce('afterRender', () => {
            this.set('inputId', this.$('input').attr('id'));
        });
    }
});
