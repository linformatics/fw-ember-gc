import Component from 'ember-component';
import PopoverMixin from 'fw-ember/mixins/popover';

/**
 * Icon that comes with the popover mixin
 *
 * @class PopoverIcon
 * @extends Ember.Component
 * @uses PopoverMixin
 * @module fw-ember
 */
export default Component.extend(PopoverMixin, {
    /**
     * Font-Awesome icon name
     * @property icon
     */
    icon: 'fa-info',

    tagName: 'i',
    classNames: ['fa'],
    classNameBindings: ['icon']

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */

    /**
     * Title of the popover
     * @property title
     */

    /**
     * Content of the popover
     * @property content
     */

    /**
     * Trigger of the popover (hover, click, or focus)
     * @property popoverTrigger
     */
});
