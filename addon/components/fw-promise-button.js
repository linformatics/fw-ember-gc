import Component from 'ember-component';
import computed from 'ember-computed';
import {cancel, later} from 'ember-runloop';
import {InvokeActionMixin} from 'ember-invoke-action';
import layout from 'fw-ember/templates/components/fw-promise-button';

/**
 * Promise-enabled buttons. PromiseButton is a component that will take a promise,
 * render as a button, and when clicked, will trigger the promise and then show its state
 * once the promise has either resolved or rejected. To read more on promises go
 * [here](https://guides.emberjs.com/v2.4.0/routing/asynchronous-routing/#toc_a-word-on-promises).
 *
 * Usage:
 * ```handlebars
 * {{#fw-promise-button class="btn btn-sm" promise=(action 'callServer')}}
 *     Click Me!
 * {{/fw-promise-button}}
 * ```
 *
 * The `promise` variable is a closure action that must return a promise. If not,
 * unexpected behavior may occur. The promise option is the only one that _needs_
 * to be specified.
 *
 * @class PromiseButton
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend(InvokeActionMixin, {
    layout,
    tagName: '',

    /**
     * Promise function (either using a string or a closure function)
     *
     * @property promise
     * @type String|Function
     */

    /**
     * If you are rendering the promise button as an inline component and not
     * in block form, set this to whatever text you want the button to display.
     *
     * @property buttonText
     * @type {String}
     * @default ''
     */
    buttonText: '',

    /**
     * If you want a button-icon to be displayed, put any of the font-awesome icons
     * as a string here. _Note: you must include the 'fa-' before it. For example,
     * to display the check icon, set this to 'fa-check'_
     *
     * @property buttonIcon
     * @type {String}
     * @default ''
     */
    buttonIcon: '',

    /**
     * This is the style of the button. This corresponds to button states in Bootstrap.
     * Can be set to 'primary', 'default', 'success', 'danger', 'info', 'warning', or 'link'
     *
     * @property buttonStyle
     * @type {String}
     * @default 'primary'
     */
    buttonStyle: 'primary',

    /**
     * Sets the icon to be shown when the promise is resolving. See
     * [here](http://fontawesome.io/examples/#animated) for a list
     * of possible icons.
     *
     * @property spinIcon
     * @type {String}
     * @default 'fa-refresh'
     */
    spinIcon: 'fa-refresh',

    /**
     * Boolean value that tells whether or not the button's width needs to stay
     * constant regardless of state.
     *
     * @property autoWidth
     * @type {Boolean}
     * @default true
     */
    autoWidth: true,

    /**
     * External control for the disabled state of the button. This is different
     * than the regular `disabled` setting because the state of the button
     * will automatically modify whether or not the button is spinning based on
     * whether or not the promise is in the process of running.
     *
     * @property disableWhen
     * @type {Boolean}
     * @default false
     */
    disableWhen: false,

    /**
     * Holds the state of the button. Can be an empty object (no state, promise has
     * not been clicked), rejected (promse has rejected), or resolved (promise has resolved)
     *
     * @private
     * @property state
     * @type {Object}
     */
    state: {},

    /**
     * The time that the state of the button should be shown before returning to
     * default. Time must be specified in milliseconds.
     *
     * @property timeout
     * @type {Number}
     * @default 3000
     */
    timeout: 3000,

    /**
     * Private instance variable holding whether or not the button is currently
     * submitting the promise. Used to modify the spinning state of `fw-spin-button`
     *
     * @private
     * @property submitting
     * @type {Boolean}
     */
    submitting: false,

    /**
     * Holds the Ember.run function responsible for resetting the state of a button
     * after the specified `timeout`.
     *
     * @private
     * @property showStateTimeout
     */
    showStateTimeout: null,

    buttonState: computed('buttonStyle', 'state', function () {
        let state = this.get('state');

        if (state.rejected) {
            return 'danger';
        } else if (state.resolved) {
            return 'success';
        } else {
            return this.get('buttonStyle');
        }
    }),

    concatenatedClasses: computed('buttonState', 'class', function () {
        let classes = [this.get('class')];

        classes.push(`btn-${this.get('buttonState')}`);

        return classes.join(' ');
    }),

    _showState(state) {
        this.set('state', state);

        this.set('showStateTimeout', later(this, function () {
            this.set('state', {});
            this.set('showStateTimeout', null);
        }, this.get('timeout')));
    },

    willDestroy() {
        this._super(...arguments);
        cancel(this.get('showStateTimeout'));
    },

    actions: {
        click() {
            if (this.get('promise')) {
                this.set('submitting', true);
                this.set('state', {});

                this.invokeAction('promise').then(() => {
                    this.set('submitting', false);
                    this._showState({resolved: true});
                    this.invokeAction('onsuccess', ...arguments);
                }).catch(() => {
                    this.set('submitting', false);
                    this._showState({rejected: true});
                    this.invokeAction('onerror', ...arguments);
                });
            }
        }
    }
});
