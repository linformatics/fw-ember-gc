import Component from 'ember-component';
import service from 'ember-inject/service';
import AuthComponentMixin from 'fw-ember/mixins/auth-component';
import {invokeAction} from 'ember-invoke-action';

/**
 * This component is used to show or hide various behavior
 * based on a certain permission level or department.
 *
 * Basic Usage:
 *
 * ```handlebars
 * {{#auth-button perm="admin" type="button" class="btn btn-sm btn-default" click=(action "click")}}
 *     Click me if you're an admin!
 * {{/auth-block}}
 * ```
 *
 * See the docs for AuthComponentMixin
 * for more details on how you can use this component.
 *
 * @class AuthButtonComponent
 * @extends Ember.Component
 * @uses AuthComponentMixin
 * @module fw-ember-gc
 */
export default Component.extend(AuthComponentMixin, {
    tagName: 'button',
    attributeBindings: ['disabled', 'type'],

    /**
     * Whether or not to mark the button as 'disabled'
     * See <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-disabled">here</a>
     * for more details
     *
     * @property disabled
     * @type {Boolean}
     */
    disabled: false,

    /**
     * What the type of the button is
     * See <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type">here</a>
     * for more details
     *
     * @property type
     * @type {String}
     */
    type: 'button',

    session: service(),
    user: service('current-user'),

    /**
     * Action to be called when the button is clicked.
     *
     * @property click
     * @type {Action}
     */
    click() {
        invokeAction(this, 'click');
    }
});
