import Component from 'ember-component';

/**
 * Basic navigation component
 *
 * Usage:
 *
 * ```handlebars
 * {{#fw-nav}}
 *     <ul>
 *         <li>
 *             {{#link-to "index"}}Index{{/link-to}}
 *     </ul>
 * {{/fw-nav}}
 * ```
 *
 * It's designed so that you can show/hide the links any way you want. For example,
 * if you wanted to only show a link based on if a user was logged in, you could wrap
 * it in an `{{auth-block}}` component:
 *
 * ```handlebars
 * {{#fw-nav}}
 *     <ul>
 *         {{#auth-block tagName="li"}}
 *             {{#link-to "index"}}Index{{/link-to}}
 *         {{/auth-block}}
 *     </ul>
 * {{/fw-nav}}
 * ```
 *
 * Result: The `index` link only shows when the user is logged in.
 *
 * @class NavComponent
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend({
    tagName: 'nav',
    classNames: 'fw-main-nav'
});
