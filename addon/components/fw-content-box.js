import Component from 'ember-component';
import computed from 'ember-computed';
import layout from 'fw-ember/templates/components/fw-content-box';

/**
 * This component is used to generate a basic bootstrap content-box. Content-boxes
 * can have a header, footer, and a body. These individual parts can be disabled optionally.
 *
 * The component passes in a `section` variable for use in determining what
 * content goes in what part of the box. This `section` variable will have one of three
 * different values: "header", "body", or "footer".
 *
 * Basic Usage:
 *
 * ```handlebars
 * {{#fw-content-box as |section|}}
 *     {{#if (eq section 'header')}}
 *         <p>Header Content</p>
 *     {{else if (eq section 'body')}}
 *         <p>Body Content</p>
 *     {{else if (eq section 'footer')}}
 *         <p>Footer Content</p>
 *     {{/if}}
 * {{/fw-content-box}}
 * ```
 *
 * To disable individual sections, you may modify the hasHeader, hasFooter, or
 * hasBody attributes of the component:
 *
 * ```handlebars
 * {{#fw-content-box hasHeader=false as |section|}}
 *     {{!same as other box}}
 * {{/fw-content-box}}
 * ```
 *
 * This is useful if you don't have any content to show in a certain part of the
 * box, or you want to hide different parts based on permissions. values, etc.
 *
 * @class ContentBox
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend({
    layout,
    classNames: 'panel',
    classNameBindings: ['panelClass'],

    panelClass: computed('type', function () {
        let type = this.get('type');

        return `panel-${type}`.htmlSafe();
    }),

    /**
     * The `type` is used to define the panel type. Can be 'default', 'primary',
     * 'success', 'danger', 'info', etc. See bootstrap docs for more details.
     * @property type
     * @type {String}
     */
    type: 'default',

    /**
     * Used to determine whether or not the header of the content box should be rendered.
     * @property hasHeader
     * @type {Boolean}
     */
    hasHeader: true,

    /**
     * Used to determine whether or not the body of the content box should be rendered.
     * @property hasBody
     * @type {Boolean}
     */
    hasBody: true,

    /**
     * Used to determine whether or not the footer of the content box should be rendered.
     * @property hasFooter
     * @type {Boolean}
     */
    hasFooter: true,

    /**
     * Adds the overflow-x: auto property to the body of the content-box
     * in the case that fixed width content is greater than the width of the box.
     *
     * @property scrollable
     * @type {Boolean}
     */
    scrollable: false
});
