import Component from 'ember-component';
import TooltipMixin from 'fw-ember/mixins/tooltip';
import {InvokeActionMixin} from 'ember-invoke-action';

/**
 * Button that comes with the tooltip mixin
 *
 * @class TooltipButton
 * @extends Ember.Component
 * @uses TooltipMixin
 * @uses InvokeActionMixin
 * @module fw-ember
 */
export default Component.extend(TooltipMixin, InvokeActionMixin, {
    tagName: 'button',

    /**
     * Either a string action name or a closure action to call when the
     * button is clicked.
     *
     * @property action
     */

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */

    /**
     * text of the popover
     * @property text
     */

    /**
     * Captures the click of a button and calls an optional "action" property
     *
     * @method click
     */
    click() {
        this.invokeAction('action');
    }
});
