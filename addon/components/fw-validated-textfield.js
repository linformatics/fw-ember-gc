import Component from 'ember-component';
import {scheduleOnce} from 'ember-runloop';
import ValidatedInputMixin from 'fw-ember/mixins/validated-input';
import {InvokeActionMixin} from 'ember-invoke-action';
import layout from 'fw-ember/templates/components/fw-validated-textfield';

export default Component.extend(InvokeActionMixin, ValidatedInputMixin, {
    layout,
    classNames: 'form-group',
    classNameBindings: 'state',

    inputId: '',
    type: 'text',
    name: '',
    placeholder: null,
    label: '',
    helpText: '',
    disabled: false,

    // other options
    readonly: null,
    size: null,
    maxlength: null,
    autocomplete: null,
    autofocus: null,
    novalidate: null,
    min: null,
    max: null,
    pattern: null,

    didInsertElement() {
        this._super(...arguments);

        scheduleOnce('afterRender', () => {
            this.set('inputId', this.$('input').attr('id'));
        });
    }
});
