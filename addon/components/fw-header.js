import Component from 'ember-component';
import service from 'ember-service/inject';
import computed from 'ember-computed';
import {assert} from 'ember-metal/utils';
import layout from 'fw-ember/templates/components/fw-header';

/**
 * Page Header Component. Used to render important parts of the header such as
 * the app switcher, the department switcher and various session controls.
 *
 * Usage:
 * ```handlebars
 * {{fw-header}}
 * ```
 *
 * Optional: pass in `showAboutLink=true` and a menu item will appear linking to
 * a modal with some basic info about the app: its version and developers.
 *
 * @class Header
 * @extends Ember.Component
 * @module fw-ember
 */
export default Component.extend({
    layout,
    tagName: 'header',
    classNames: ['fw-header', 'clearfix'],

    user: null,

    /**
     * Whether or not to show a menu item linking to a component that shows
     * basic app data.
     *
     * @property showAboutLink
     * @type {Boolean}
     */
    showAboutLink: false,

    // Modal show flags
    showAppsModal: false,
    showDeptModal: false,
    showAboutModal: false,
    showChangeModal: false,

    /**
     * Whether or not the logged in user needs a password change
     *
     * @private
     * @property needSecureChangePassword
     * @type {Boolean}
     */
    needSecureChangePassword: false,

    // Services
    config: service(),
    currentUser: service(),
    session: service(),
    notifications: service(),

    // Hooks to the currentUser service to open the change password modal when needed
    init() {
        this._super(...arguments);
        this.get('currentUser').on('change-password', this, function (needSecure) {
            this.send('toggleChangeModal', needSecure);
        });
    },

    logo: computed(function () {
        assert('Logo must be a property set on the config service', this.get('config.logo'));
        return this.get('config.logo') || 'http://placehold.it/45x45/';
    }),

    actions: {
        logout() {
            this.get('session').invalidate();
        },

        reload() {
            this.get('currentUser').reload().catch(() => {
                this.get('notifications').showError('Reload failed!', 'global');
            });
        },

        toggleAppsModal() {
            this.toggleProperty('showAppsModal');
        },

        toggleDeptModal() {
            this.toggleProperty('showDeptModal');
        },

        toggleAboutModal() {
            this.toggleProperty('showAboutModal');
        },

        toggleChangeModal(needSecure = false) {
            this.toggleProperty('showChangeModal');
            this.set('needSecureChangePassword', needSecure);
        }
    }
});
