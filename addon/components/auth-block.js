import Component from 'ember-component';
import AuthComponentMixin from 'fw-ember/mixins/auth-component';

/**
 * This component is used to show or hide various behavior
 * based on a certain permission level or department.
 *
 * Basic Usage:
 *
 * ```handlebars
 * {{#auth-block perm="admin"}}
 *     <button {{action "someAction"}}>
 *         Click me if you're an admin!
 *     </button>
 * {{/auth-block}}
 * ```
 *
 * See the docs for AuthComponentMixin
 * for more details on how you can use this component.
 *
 * @class AuthBlockComponent
 * @extends Ember.Component
 * @uses AuthComponentMixin
 * @module fw-ember-gc
 */
export default Component.extend(AuthComponentMixin);
