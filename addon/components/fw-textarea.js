import TextArea from 'ember-components/text-area';

/**
 * An extension of Ember's native `{{textarea}}` component, but
 * applies Bootstrap styling. See Ember's `{{textarea}}`
 * [documentation](https://guides.emberjs.com/v2.5.0/templates/input-helpers/)
 * for more information.
 *
 * @class FWTextarea
 * @extends Ember.TextArea
 * @module fw-ember
 */
export default TextArea.extend({
    classNames: 'form-control'
});
