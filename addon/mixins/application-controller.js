import Mixin from 'ember-metal/mixin';
import service from 'ember-service/inject';

/**
 * Mixin that the application controller needs to use in order
 * to be able to use certain things like the login modal functionality
 *
 * Usage: (in controllers/application.js)
 * ```javascript
 * import Ember from 'ember';
 * import ApplicationControllerMixin from 'fw-ember/mixins/application-controller';
 *
 * export default Ember.Controller.extend(ApplicationControllerMixin, {
 *     // more code here
 * });
 * ```
 *
 * @class ApplicationControllerMixin
 * @extends Ember.Mixin
 * @module fw-ember-gc
 */
export default Mixin.create({
    showLoginModal: false,
    attemptedTransition: null,

    session: service(),

    init() {
        this._super(...arguments);
        this.get('session').on('re-login', this, function (attemptedTransition) {
            this.send('openLoginModal', attemptedTransition);
        });
    },

    actions: {
        openLoginModal(attemptedTransition = null) {
            if (attemptedTransition) {
                this.set('attemptedTransition', attemptedTransition);
            }
            this.set('showLoginModal', true);
        },

        closeLoginModal() {
            this.set('attemptedTransition', null);
            this.set('showLoginModal', false);
        }
    }
});
