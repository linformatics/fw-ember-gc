import Mixin from 'ember-metal/mixin';

/**
 * Mixin that enables tooltip function
 *
 * @class TooltipMixin
 * @extends Ember.Mixin
 * @module fw-ember
 */
export default Mixin.create({
    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */
    placement: 'top',

    /**
     * text of the popover
     * @property text
     */
    text: '',

    toggle: 'tooltip',

    attributeBindings: [
        'placement:data-placement',
        'toggle:data-toggle'
    ],

    didInsertElement() {
        this._super(...arguments);
        this.$().tooltip({
            title: this.get('text'),
            container: 'body'
        });
    },

    willDestroyElement() {
        this._super(...arguments);
        this.$().tooltip('destroy');
    }
});
