import Mixin from 'ember-metal/mixin';
import service from 'ember-service/inject';

/**
 * A mixin designed to be used by routes that wish to check if the user
 * is still logged in before transitioning to a new route.
 *
 * Usage: (in a routes/*.js file)
 * ```javascript
 * import Ember from 'ember';
 * import AuthCheckMixin from 'fw-ember/mixin/auth-check';
 *
 * export default Ember.Route.extend({
 *     // route code here
 * });
 * ```
 *
 * @class AuthCheckMixin
 * @extends Ember.Mixin
 * @module fw-ember-gc
 */
export default Mixin.create({
    currentUser: service(),
    session: service(),

    actions: {
        willTransition(transition) {
            return this.get('currentUser').check().then((isActive) => {
                if (!isActive) {
                    transition.abort();
                    this.get('session').triggerReAuthenticate(transition);
                } else {
                    return this._super(...arguments);
                }
            });
        }
    }
});
