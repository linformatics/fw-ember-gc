import Mixin from 'ember-metal/mixin';
import RSVP from 'rsvp';

/**
 * This mixin handles some behavior on controllers or routes that are editing models.
 * If the model is dirty (e.g. has been changed since the route was entered),
 * this mixin adds functionality that warns users about potentially losing data.
 * If the user then chooses to leave this mixin handles reverting the changes
 * back to their previous state.
 *
 * Designed for use in routes, but can be applied on controllers
 *
 * @class RollbackMixin
 * @extends Ember.Mixin
 * @module fw-ember
 */
export default Mixin.create({
    activate() {
        this._super(...arguments);

        window.onbeforeunload = () => {
            return this.isDirty() ? this._rollbackMessage() : null;
        };
    },

    _rollback() {
        let model = this.get('currentModel') || this.get('model');

        if (model.get('isNew') && !model.get('isSaving')) {
            model.deleteRecord();
        } else if (model.get('hasDirtyAttributes') && !model.get('isSaving')) {
            model.rollbackAttributes();
        }

        window.onbeforeunload = null;
    },

    _rollbackMessage() {
        return '===================================\n\n' +
            'WARNING: You haven\'t saved your changes.\n' +
            'Please save them if you wish to retain your data.\n\n' +
            '===================================';
    },

    isDirty() {
        let model = this.get('currentModel') || this.get('model');

        if (model.get('isNew') && !model.get('isSaving')) {
            return Object.keys(model.changedAttributes()).length > 0;
        }

        return (model.get('hasDirtyAttributes') && !model.get('isSaving'));
    },

    actions: {
        willTransition(transition) {
            let superCall = this._super(transition);

            if (this.isDirty()) {
                if (!confirm('Are you sure you want to change the page? You have unsaved changes.')) {
                    transition.abort();
                } else {
                    this._rollback();
                }

                return RSVP.resolve(superCall);
            } else {
                this._rollback();

                return RSVP.resolve(superCall);
            }
        }
    }
});
