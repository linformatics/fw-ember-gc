import Mixin from 'ember-metal/mixin';

/**
 * Mixin that enables popover functionality
 *
 * @class PopoverMixin
 * @extends Ember.Mixin
 * @module fw-ember-gc
 */
export default Mixin.create({
    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */
    placement: 'top',

    /**
     * Title of the popover
     * @property title
     */
    title: '',

    /**
     * Content of the popover
     * @property content
     */
    content: '',

    /**
     * Trigger of the popover (hover, click, or focus)
     * @property popoverTrigger
     */
    popoverTrigger: 'hover',

    toggle: 'popover',

    attributeBindings: [
        'placement:data-placement',
        'toggle:data-toggle'
    ],

    didInsertElement() {
        this._super(...arguments);
        this.$().popover({
            title: this.get('title'),
            content: this.get('content'),
            trigger: this.get('popoverTrigger'),
            container: 'body'
        });
    },

    willDestroyElement() {
        this._super(...arguments);
        this.$().tooltip('destroy');
    }
});
