import Mixin from 'ember-metal/mixin';
import computed from 'ember-computed';
import {isEmpty} from 'ember-utils';
import service from 'ember-service/inject';

/**
 * Basic mixin to be implemented in various components
 * that enables showing or hiding it based on auth permissions
 *
 * To use in a component:
 * ```javascript
 * // app/components/some-component.js
 * import Ember from 'ember';
 * import AuthComponentMixin from 'fw-ember/mixins/auth-component';
 *
 * export default Ember.Component.extend(AuthComponentMixin, {
 *     // your code here....
 * });
 * ```
 *
 * @class AuthComponentMixin
 * @module fw-ember-gc
 */
export default Mixin.create({
    /**
     * @property defaultVisibility
     * @type {Boolean}
     */
    defaultVisibility: true,

    /**
     * Permission value. This will be passed directly to the {{#crossLink match}}{{/crossLink}}
     * function.
     *
     * @property perm
     * @type {String|Array}
     */
    perm: '',

    /**
     * If supplied, the mixin checks this against a user's departments.
     *
     * @property dept
     * @type {String}
     */
    dept: '',

    /**
     * If supplied, this will be checked against the user's roles using
     * the {{#crossLink match}}{{/crossLink}} function. If it passes,
     * it will ignore the `dept` property.
     * @type {String}
     */
    override: '',

    currentUser: service(),
    session: service(),

    isVisible: computed('session.isAuthenticated', 'user.roles', 'perm', 'dept', 'defaultVisibility', function () {
        let user = this.get('currentUser');

        if (!this.get('session.isAuthenticated')) {
            return false;
        }

        if (this.get('override') && user.match(this.get('override'))) {
            return true;
        }

        if (isEmpty(this.get('perm'))) {
            if (this.get('dept')) {
                return user.checkDepartment(this.get('dept'));
            }
            return this.get('defaultVisibility');
        }

        return user.match(this.get('perm')) && (this.get('dept') ? user.checkDepartment(this.get('dept')) : true);
    })
});
