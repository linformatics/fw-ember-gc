import Mixin from 'ember-metal/mixin';
import computed, {reads, alias, not, and} from 'ember-computed';
import {defineProperty} from 'ember-platform';

/**
 * Mixin that adds functionality to validation-enabled components
 *
 * @class ValidatedInputMixin
 * @module fw-ember
 */
export default Mixin.create({
    valuePath: '',
    validation: null,
    value: null,
    model: null,
    disableStyle: false,

    didReceiveAttrs() {
        this._super(...arguments);

        let valuePath = this.get('valuePath');

        defineProperty(this, 'validation', reads(`model.validations.attrs.${valuePath}`));
        defineProperty(this, 'value', alias(`model.${valuePath}`));
        defineProperty(this, 'didValidate', computed('model.hasValidated.[]', function () {
            return this.get('model.hasValidated').contains(valuePath);
        }));
    },

    concatenatedClasses: computed('classes', function () {
        let classes = [this.get('classes')];
        classes.push('form-control');
        return classes.join(' ');
    }),

    notValidating: not('validation.isValidating'),
    isValid: and('validation.isValid', 'notValidating', 'didValidate'),
    isInvalid: reads('validation.isInvalid'),
    hasError: and('notValidating', 'showErrorMessage', 'validation'),
    showErrorMessage: and('didValidate', 'isInvalid', 'notValidating'),

    state: computed('isValid', 'hasError', 'didValidate', 'disableStyle', function () {
        if (!this.get('didValidate') || this.get('disableStyle')) {
            return '';
        }

        if (this.get('hasError')) {
            return 'has-error';
        }

        if (this.get('isValid')) {
            return 'has-success';
        }
    }),

    actions: {
        update(value) {
            let valuePath = this.get('valuePath');

            this.get('model.hasValidated').removeObject(valuePath);
            this.invokeAction('update', value);
        },

        focusOut() {
            let valuePath = this.get('valuePath');
            this.get('model').validate({on: [valuePath]}).finally(() => {
                this.get('model.hasValidated').pushObject(valuePath);

                this.invokeAction('focus-out');
            });
        }
    }
});
