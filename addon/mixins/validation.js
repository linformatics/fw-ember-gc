import Mixin from 'ember-metal/mixin';
import {A as emberA} from 'ember-array/utils';
import RSVP from 'rsvp';

const {resolve, reject} = RSVP;

/**
 * Validation mixin that applies to models that use the ember-cp-validations mixin
 *
 * @class ValidationMixin
 * @module fw-ember
 */
export default Mixin.create({
    hasValidated: null,

    init() {
        this._super(...arguments);

        this.set('hasValidated', emberA());
    },

    validate() {
        return this.get('validations').validate(...arguments).then(({validations}) => {
            this.get('hasValidated').pushObjects(validations.get('content').getEach('attribute'));

            return (validations.get('isValid')) ? resolve() : reject();
        });
    },

    save(options) {
        let {_super} = this;

        options = options || {};

        return this.validate().then(() => {
            return _super.call(this, options);
        });
    },

    clearValidations() {
        this.get('hasValidated').clear();
    }
});
