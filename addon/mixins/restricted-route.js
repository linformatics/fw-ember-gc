import Ember from 'ember';

/**
 * @module fw-ember-gc
 */
const {
    Mixin,
    inject: {service}
} = Ember;

/**
 * Mixin that encapsulates some of the route restriction behavior.
 *
 * If a roles array and a rejectedRoute property are specified, this mixin
 * will ensure the user has adequate permissions before proceeding to the route.

 * Mixin designed to be used by a route. Routes that use this mixin **MUST**
 * also use the PermissionsMixin
 *
 * Usage: (in a routes file)
 * ```javascript
 * import Ember from 'ember';
 * import PermissionsMixin from 'fw-ember/mixins/permissions';
 * import RestrictedRouteMixin from 'fw-ember/mixins/restricted-route';
 *
 * export default Ember.Route.extend(PermissionsMixin, RestrictedRouteMixin, {
 *     // your code here
 * });
 * ```
 *
 * @class RestrictedRouteMixin
 * @extends Ember.Mixin
 */
export default Mixin.create({
    /**
     * Array of roles to be passed to the match function
     * @property roles
     * @type {Array}
     */
    roles: [],

    /**
     * Route to transition to if the check fails
     *
     * @property rejectedRoute
     * @type {String}
     */
    rejectedRoute: 'index',

    session: service(),

    beforeModel(transition) {
        let superResult = this._super(transition);

        if (this.get('session.isAuthenticated')) {
            this.permit(this.get('roles'), this.get('rejectedRoute'));
        }

        return superResult;
    }
});
