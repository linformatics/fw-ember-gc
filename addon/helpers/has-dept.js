import Helper from 'ember-helper';
import service from 'ember-service/inject';

/**
 * Used to determine if the user is part of a specific department
 * Returns a boolean value
 *
 * Usage:
 * ```handlebars
 * {{has-dept 'test'}} <!-- if the user is in the "test" department, this returns true -->
 * ```
 *
 * @class HasDept
 * @extends Ember.Helper
 * @module fw-ember-gc
 */
export default Helper.extend({
    currentUser: service(),

    compute(dept) {
        return this.get('currentUser.depts').contains(dept.get('id'));
    }
});
