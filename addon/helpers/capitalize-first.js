import {helper} from 'ember-helper';

/**
 * Helper that capitalizes the first letter of a word
 *
 * Usage:
 *
 * ```handlebars
 * {{capitalize-first "word"}} <!-- outputs "Word" -->
 * ```
 *
 * @class CapitalizeFirstHelper
 * @extends Ember.Helper
 * @module fw-ember
 */
export default helper(function (arr) {
    if (arr.length > 0 && arr[0]) {
        let [string] = arr;

        if (typeof string !== 'string') {
            return string;
        }

        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    return '';
});
