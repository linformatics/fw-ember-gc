import Helper from 'ember-helper';
import service from 'ember-service/inject';

/**
 * Checks if a user has a specific role. Uses the {{#crossLink match}}{{/crossLink}}
 * utility, so anything passed in for the role will be passed to the match function.
 *
 * Usage:
 * ```handlebars
 * <!-- if the match function returns true this will return true -->
 * {{has-role role}}
 * ```
 *
 * @class HasRole
 * @extends Ember.Helper
 * @module fw-ember-gc
 */
export default Helper.extend({
    currentUser: service(),

    compute(role) {
        return this.get('currentUser').match(role);
    }
});
