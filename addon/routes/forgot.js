import BaseRoute from './base';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

// Route for the forgotten password route.
export default BaseRoute.extend(UnauthenticatedRouteMixin, {
    title: 'Forgot Password',

    setupController(controller) {
        this._super(...arguments);
        let router = this.get('router');
        let url = window.location.origin + router.get('rootURL') + router.generate('reset');

        controller.set('resetUrl', url);
        return this._super(...arguments);
    },

    deactivate() {
        let controller = this.get('controller');

        this._super(...arguments);

        controller.setProperties({
            email: '',
            resetUrl: ''
        });
    }
});
