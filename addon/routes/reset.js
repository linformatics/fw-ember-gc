import BaseRoute from './base';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

// Reset password route handler
export default BaseRoute.extend(UnauthenticatedRouteMixin, {
    model(params, transition) {
        if (!params.token) {
            transition.abort();
            this.transitionTo('login');
        }
    },

    deactivate() {
        let controller = this.get('controller');

        this._super(...arguments);

        controller.setProperties({
            password: '',
            confirmPassword: ''
        });
    }
});
