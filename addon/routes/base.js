import $ from 'jquery';
import Route from 'ember-route';
import service from 'ember-service/inject';
import {isEmpty} from 'ember-utils';

/**
 * Base route that handles html title modification in particular routes
 *
 * Usage: (in a routes file)
 * ```javascript
 * import BaseRoute from 'fw-ember/routes/base';
 *
 * export default BaseRoute.extend({
 *     // Your code here
 * });
 * ```
 *
 * @class BaseRoute
 * @extends Ember.Route
 * @module fw-ember
 */
export default Route.extend({
    config: service(),

    activate() {
        this._super(...arguments);

        let appName = this.get('config.name');
        let title = (isEmpty(this.get('title'))) ? appName : `${this.get('title')} | ${appName}`;

        $(document).attr('title', title);
    }
});
