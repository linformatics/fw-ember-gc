import EmberObject from 'ember-object';
import getOwner from 'ember-owner/get';
import BaseRoute from './base';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

import LoginValidation from 'fw-ember/mixins/validations/login';
import ValidationMixin from 'fw-ember/mixins/validation';

const LoginObject = EmberObject.extend(LoginValidation, ValidationMixin, {
    identification: '',
    password: '',

    clear() {
        this.setProperties({
            identification: '',
            password: ''
        });
    }
});

// Login route handler
export default BaseRoute.extend(UnauthenticatedRouteMixin, {
    title: 'Login',

    model() {
        return LoginObject.create(getOwner(this).ownerInjection());
    },

    deactivate() {
        let currentModel = this.modelFor(this.routeName);
        this._super(...arguments);
        currentModel.clear();
    }
});
