/**
 * Handles the matching of a given set of roles to a flat list of roles.
 * @class match
 * @module fw-ember-gc
 */

function singleMatch(needle, haystack) {
    let i, j, bool, hay, straw;

    if (typeof haystack !== 'object') {
        return needle === haystack;
    }
    for (i = 0; i < haystack.length; i++) {
        hay = haystack[i];
        if (typeof hay === 'object') {
            bool = false;
            for (j = 0; j < hay.length; j++) {
                straw = hay[j];
                if (needle === straw) {
                    bool = true;
                    break;
                }
            }
            if (!bool) {
                return false;
            }
        } else {
            if (needle !== hay) {
                return false;
            }
        }
    }
    return true;
}

/**
 * Matches a role/list of roles (`haystack`) to a flat list of
 * available roles (`needle`)
 *
 * How this function works: the roles/list of roles passed in via `haystack`
 * are matched using an 'and/or' setup. If `haystack` is an array, it treats each role
 * in `haystack` as an "AND" requirement. If roles is an array and one of the items in
 * that array is a sub-array of roles, that sub-array is treated with an "OR" rule.
 *
 * Example:
 * ```javascript
 * match(needle, ['a', 'b']); // needle must contain both 'a' AND 'b'
 * match(needle, [['a', 'b']]); // needle must contain either 'a' OR 'b'
 * match(needle, [['a', 'b'], 'c']); // needle must container 'c' AND either 'a' OR 'b'
 * ```
 *
 * @method match
 * @param {Array} needle flat array of available roles
 * @param {Array|String} haystack array of role rules to check
 */
export default function (needle, haystack) {
    let i;

    if (typeof needle === 'object') {
        for (i = 0; i < needle.length; i++) {
            if (singleMatch(needle[i], haystack)) {
                return true;
            }
        }
        return false;
    } else {
        return singleMatch(needle, haystack);
    }
}
