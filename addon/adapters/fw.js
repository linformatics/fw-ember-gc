import service from 'ember-service/inject';
import computed from 'ember-computed';
import RESTAdapter from 'ember-data/adapters/rest';
import {AdapterError} from 'ember-data/adapters/errors';

/**
 * The fw adapter defines a base adapter for the ember-data store.
 * By default, this is included in an app as the `application` adatper; however,
 * if you should need to override/extend any behavior in your particular app,
 * you may extend this in another adapter.
 *
 * For example:
 *
 * ```javascript
 * import FwAdapter from 'fw-ember/adapters/fw';
 *
 * export default FwAdapter.extend({
 *     // ... your code here
 * });
 * ```
 *
 * Note: If you just want to extend/override the adapter for one particular
 * model, you can do so by providing an adapter for that particular model
 * without overriding the base one.
 *
 * For example, if you had a 'user' model:
 *
 * ```javascript
 * import BaseAdapter from './application';
 *
 * export default BaseAdapter.extend({
 *     // ... your code here
 * });
 * ```
 *
 * @class FwAdapter
 * @extends DS.RESTAdapter
 * @module fw-ember-gc
 */
export default RESTAdapter.extend({

    /**
     * The `config` property is an injected service that provides
     * various properties such as the api root that are used to handle
     * requests.
     *
     * @property config
     * @type ConfigService
     */
    config: service(),

    /**
     * The `notifications property is an injected service that handles alerts
     * when certain things happen such as a bad request, unauthorized request
     *
     * @property notifications
     * @type NotificationsService`
     */
    notifications: service(),

    /**
     * The `session` property is an injected service that handles client-side
     * authentication.
     *
     * @property service
     * @type SessionService
     */
    session: service(),

    /**
     * Computed alias of `config.apiRoot`
     *
     * @property host
     * @type String
     */
    host: computed.alias('config.apiRoot'),

    /**
     * Ensures trailing slashes for all urls (extends Ember-Data's method)
     *
     * @method buildURL
     * @return String
     */
    buildURL() {
        // Ensure trailing slashes
        let url = this._super(...arguments);

        if (url.slice(-1) !== '/') {
            url += '/';
        }

        return url;
    },

    // TODO: Figure out what the effects of these two methods are
    shouldReloadAll() {
        return true;
    },

    shouldBackgroundReloadRecord() {
        return false;
    },

    /**
     * Handles ajax responses (including status errors)
     *
     * @method handleResponse
     * @param  {Number} status
     * @param  {Object} headers
     * @param  {Object} payload Object
     * @return {Object | DS.AdapterError} response
     */
    handleResponse(status, headers, payload) {
        if (status === 401 && payload.error) {
            this.get('session').triggerReAuthenticate();
            return new AdapterError();
        } else if (status === 500 && payload.error) {
            this.get('notifications').showError(payload.message, 'global');
            return new AdapterError();
        }

        return this._super(...arguments);
    }
});
