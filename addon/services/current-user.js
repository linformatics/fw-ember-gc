import Service from 'ember-service';
import service from 'ember-service/inject';
import computed from 'ember-computed';
import Evented from 'ember-evented';
import RSVP from 'rsvp';
import {A as emberA} from 'ember-array/utils';
import {isEmpty} from 'ember-utils';
import match from 'fw-ember/utils/match';

const {reject} = RSVP;

/**
 * This service handles various functionality that deals with logged in
 * user management.
 *
 * @class CurrentUserService
 * @extends Ember.Service
 * @uses Ember.Evented
 * @module fw-ember-gc
 */
export default Service.extend(Evented, {
    /**
     * username of the logged in user
     * @property userId
     * @type {String}
     */
    userId: '',

    /**
     * Array of programs the user has access to
     *
     * @property programs
     * @type {Array}
     */
    programs: emberA(),

    /**
     * Array of roles the user has for the current program and department
     *
     * @property roles
     * @type {Array}
     */
    roles: emberA(),

    /**
     * Array of departments the user has access to for the current program
     *
     * @property depts
     * @type {Array}
     */
    depts: emberA(),

    /**
     * Current department the user is viewing the application as
     *
     * @property currentDept
     * @type {String}
     */
    currentDept: '',

    /**
     * First name of the user
     *
     * @property nameFirst
     * @type {String}
     */
    nameFirst: '',

    /**
     * Last name of the user
     *
     * @property nameLast
     * @type {String}
     */
    nameLast: '',

    /**
     * Preferred first name (nickname) of the user
     *
     * @property namePref
     * @type {String}
     */
    namePref: '',

    /**
     * Email of the user
     *
     * @property email
     * @type {String}
     */
    email: '',

    config: service(),
    ajax: service(),

    /**
     * Full name of the user, using the user's first, last, and
     * preferred name (if available)
     *
     * @property nameFull
     * @type {String}
     */
    nameFull: computed('nameFirst', 'nameLast', 'namePref', function () {
        if (isEmpty(this.get('nameFirst')) || isEmpty(this.get('nameLast'))) {
            return '';
        }
        return `${(this.get('namePref') ? this.get('namePref') : this.get('nameFirst'))} ${this.get('nameLast')}`;
    }),

    /**
     * Checks if the user is currently viewing the application as
     * a particular department
     *
     * @method checkDepartment
     * @param  {String} dept department to check
     * @return {Boolean}     whether or not the current department matches the provided one
     */
    checkDepartment(dept) {
        return this.get('currentDept') === dept;
    },

    /**
     * Changes the department the user is currently viewing the department as.
     *
     * @method changeDepartment
     * @param  {String} newDept new department to view
     * @return {Promise}        Promise that resolves when the request has been made successfully
     */
    changeDepartment(newDept) {
        let config = this.get('config');

        return this.get('ajax').request(config.formUrl('session', 'change', {program: config.get('programId'), dept: newDept, app: 'gc'}))
            .then((data) => {
                this.setProperties(data);
            });
    },

    /**
     * Checks if the user is still logged in.
     *
     * @method check
     * @return {Promise} Promise that resolves with the current user's status
     */
    check() {
        let config = this.get('config');

        return this.get('ajax').request(config.formUrl('session', 'check', {app: 'gc'})).then((data) => {
            return data.isActive;
        }).catch(() => {
            return false;
        });
    },

    /**
     * Reloads the user's roles
     *
     * @method reload
     * @return {Promise} Promise that resolves when the request has completed.
     */
    reload() {
        let config = this.get('config');
        let program = config.get('programId');
        let app = 'gc';

        return this.get('ajax').request(config.formUrl('session', 'reload', {program, app})).then(() => {
            window.location.replace(this.get('config.url'));
        }).catch(() => {
            return reject();
        });
    },

    /**
     * Checks if the user's roles match the provided list of roles.
     * Uses the {{#crossLink match}}{{/crossLink}}
     * @param  {Array|String} roles Roles to check
     * @return {Boolean}      Whether or not the given roles match the user's roles
     */
    match(roles) {
        return match(this.get('roles'), roles);
    },

    /**
     * Shortcut to trigger the change-password trigger
     *
     * @private
     * @method changePassword
     * @param {Boolean} needSecure Whether or the user needs to change this password for security reasons.
     */
    changePassword(needSecure = false) {
        this.trigger('change-password', needSecure);
    }
});
