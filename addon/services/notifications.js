import Service from 'ember-service';
import Evented from 'ember-evented';
import {A as emberA} from 'ember-array/utils';

/**
 * Service that handles the rendering of notifications
 *
 * @class NotificationsService
 * @extends Ember.Service
 * @uses Ember.Evented
 * @module fw-ember
 */
export default Service.extend(Evented, {
    /**
     * All of the notifications currently in the DOM
     * @private
     * @property content
     * @type {Array}
     */
    content: emberA(),

    /**
     * Shows an informational message
     *
     * @method showInfo
     * @param  {String} message     message to show
     * @param  {String} outlet      outlet to render notification into
     * @param  {Boolean} dismissable Whether or not the notification is dismissable
     */
    showInfo(message, outlet, dismissable) {
        this.handleNotification({
            type: 'info',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows a warning message
     *
     * @method showWarning
     * @param  {String} message     message to show
     * @param  {String} outlet      outlet to render notification into
     * @param  {Boolean} dismissable Whether or not the notification is dismissable
     */
    showWarning(message, outlet, dismissable) {
        this.handleNotification({
            type: 'warning',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows an error message
     *
     * @method showError
     * @param  {String} message     message to show
     * @param  {String} outlet      outlet to render notification into
     * @param  {Boolean} dismissable Whether or not the notification is dismissable
     */
    showError(message, outlet, dismissable) {
        this.handleNotification({
            type: 'danger',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows a success message
     *
     * @method showSuccess
     * @param  {String} message     message to show
     * @param  {String} outlet      outlet to render notification into
     * @param  {Boolean} dismissable Whether or not the notification is dismissable
     */
    showSuccess(message, outlet, dismissable) {
        this.handleNotification({
            type: 'success',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Handles a raw notification
     *
     * @method handleNotification
     * @param  {Object} message options for the notification
     */
    handleNotification(message) {
        message.type = message.type || 'info';
        message.outlet = message.outlet || 'default';

        if (message.type === 'error') {
            message.type = 'danger';
        }

        this.get('content').pushObject(message);
    },

    /**
     * Close a notification given by a particular object
     *
     * @method closeNotification
     * @param  {Object} message notification object
     */
    closeNotification(message) {
        this.get('content').removeObject(message);
    },

    /**
     * Close all notifications for a particular outlet
     *
     * @method closeAllNotifications
     * @param  {String} outlet outlet to close notifications in
     */
    closeAllNotifications(outlet) {
        if (outlet) {
            this.set('content', this.get('content').filter((item) => {
                return item.outlet !== outlet;
            }));
        } else {
            this.get('content').clear();
        }
    }
});
