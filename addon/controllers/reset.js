import Controller from 'ember-controller';
import service from 'ember-service/inject';
import {
    ForbiddenError,
    isBadRequestError
} from 'ember-ajax/errors';

import {validator, buildValidations} from 'ember-cp-validations';
import ValidationMixin from 'fw-ember/mixins/validation';

const ResetValidator = buildValidations({
    password: [
        validator('presence', {
            presence: true,
            allowBlank: true,
            message: 'You must supply a password.'
        })
    ],
    confirmPassword: [
        validator('confirmation', {
            on: 'password',
            message: 'Passwords do not match.'
        })
    ]
});

/**
 * Reset Password controller
 */
export default Controller.extend(ResetValidator, ValidationMixin, {
    password: '',
    confirmPassword: '',

    queryParams: ['token'],
    token: '',

    notifications: service(),
    config: service(),
    session: service(),
    ajax: service(),

    actions: {
        resetPassword() {
            let resetUrl = this.get('config').formUrl('session', 'reset', {app: 'gc'});

            this.get('notifications').closeAllNotifications('reset');

            return this.validate().then(() => {
                return this.get('ajax').put(resetUrl, {
                    data: {
                        new: this.get('password'),
                        confirm: this.get('confirmPassword'),
                        token: this.get('token')
                    }
                }).then((response) => {
                    let data;
                    if (response && response.id) {
                        data = {user: response.id, pass: this.get('password')};

                        return this.get('session').authenticate('authenticator:group-control', data.user, data.pass).then(() => {
                            this.get('notifications').closeAllNotifications('reset');
                        }).catch((e) => {
                            if (e instanceof ForbiddenError) {
                                this.get('notifications').showError('You do not have permission to access this application', 'reset');
                            }
                        });
                    } else {
                        this.get('notifications').showError(response.message || 'An error occured.', 'reset');
                    }
                }).catch((error) => {
                    if (isBadRequestError(error)) {
                        this.get('notifications').showError(error.errors[0].detail.message, 'reset');
                    } else {
                        this.get('notifications').showError('An error occured', 'reset');
                    }
                });
            });
        }
    }
});
