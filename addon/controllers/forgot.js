import Controller from 'ember-controller';
import service from 'ember-service/inject';

import {validator, buildValidations} from 'ember-cp-validations';
import ValidationMixin from 'fw-ember/mixins/validation';

const ForgotValidator = buildValidations({
    email: [
        validator('presence', {
            presence: true,
            message: 'Please enter an email.'
        }),
        validator('format', {
            allowBlank: true,
            type: 'email',
            message: 'Email address is not valid.'
        })
    ]
});

/**
 * Controller for the forgot password page. This is automatically added
 * to every application using the fw-ember-gc blueprint so that this specific functionality
 * is re-used.
 */
export default Controller.extend(ForgotValidator, ValidationMixin, {
    email: '',
    resetUrl: '',

    config: service(),
    ajax: service(),
    notifications: service(),

    actions: {
        sendResetEmail() {
            this.get('notifications').closeAllNotifications('forgot');

            let forgotUrl = this.get('config').formUrl('session', 'forgot', {app: 'gc'});

            return this.validate().then(() => {
                return this.get('ajax').post(forgotUrl, {
                    data: {
                        email: this.get('email'),
                        url: this.get('resetUrl')
                    }
                }).then((response) => {
                    if (response && !response.error) {
                        this.get('notifications').showSuccess('If your account is in our records, an email has been sent to you.', 'forgot', true);
                    } else {
                        this.get('notifications').showError(response.message || 'An error occured.', 'forgot', true);
                    }
                }).catch((e) => {
                    this.get('notifications').showError('An error occurred.', 'forgot');

                    throw e;
                });
            });
        }
    }
});
