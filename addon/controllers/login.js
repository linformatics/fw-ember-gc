import Controller from 'ember-controller';
import service from 'ember-service/inject';
import { UnauthorizedError, ForbiddenError } from 'ember-ajax/errors';

/**
 * Controller for the login page.
 */
export default Controller.extend({
    session: service(),
    notifications: service(),

    actions: {
        login() {
            let data = this.get('model').getProperties('identification', 'password');

            this.get('notifications').closeAllNotifications('login');

            this.get('model').validate().then(() => {
                this.get('session').authenticate('authenticator:group-control', data.identification, data.password).catch((e) => {
                    if (e instanceof UnauthorizedError) {
                        this.get('notifications').showError('Username or Password incorrect.', 'login', true);
                    } else if (e instanceof ForbiddenError) {
                        this.get('notifications').showError('You do not have access to this application.', 'login', true);
                    }
                });
            });
        }
    }
});
