export default function (router) {
    router.route('login');
    router.route('forgot');
    router.route('reset');
    router.route('profile');

    router.route('error404', {path: '/*path'});
}
