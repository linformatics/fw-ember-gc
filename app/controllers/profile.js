import Controller from 'ember-controller';
import service from 'ember-service/inject';

export default Controller.extend({
    currentUser: service(),
    config: service(),
    ajax: service(),
    notifications: service(),

    actions: {
        save() {
            let model = this.get('model');
            let data = model.getProperties('nameFirst', 'nameLast', 'namePref', 'email');
            let userUrl = this.get('config').formUrl('users', this.get('currentUser.userId'), {app: 'gc'});

            return model.validate().then(() => {
                return this.get('ajax').put(userUrl, {
                    data: {
                        user: data
                    }
                }).then(({user}) => {
                    delete user.id;

                    this.get('notifications').showSuccess('Saved', 'profile', true);

                    this.get('currentUser').setProperties(user);
                    model.get('hasValidated').clear();
                }).catch(() => {
                    this.get('notifications').showError('An error occurred', 'profile', true);
                });
            });
        },

        changePassword() {
            this.get('currentUser').changePassword();
        }
    }
});
