import EmberObject from 'ember-object';
import service from 'ember-service/inject';
import getOwner from 'ember-owner/inject';
import AuthRoute from './auth';

import ProfileValidation from 'fw-ember/mixins/validations/profile';
import ValidationMixin from 'fw-ember/mixins/validation';

const CurrentUserModel = EmberObject.extend(ProfileValidation, ValidationMixin);

export default AuthRoute.extend({
    title: 'User Profile',

    user: service('current-user'),
    config: service(),
    ajax: service(),

    model() {
        let usersUrl = this.get('config').formUrl('users', this.get('user.userId'), {app: 'gc'});

        return this.get('ajax').request(usersUrl).then(({user}) => {
            return CurrentUserModel.create(getOwner(this).ownerInjection(), user);
        });
    }
});
