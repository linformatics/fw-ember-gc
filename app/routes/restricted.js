import AuthRoute from './auth';
import RestrictedRouteMixin from 'fw-ember/mixins/restricted-route';

/**
 * @module fw-ember-gc
 */

/**
 * Convenience route that bundles some of the restricted route mixins
 *
 * Usage: (in a routes file)
 * ```javascript
 * import RestrictedRoute from './restricted';
 *
 * export default RestrictedRoute.extend({
 *     // your code here...
 * });
 * ```
 * @class RestrictedRoute
 * @extends AuthRoute
 * @uses RestrictedRouteMixin
 */
export default AuthRoute.extend(RestrictedRouteMixin);
