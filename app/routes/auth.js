import BaseRoute from './base';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import AuthCheckMixin from 'fw-ember/mixins/auth-check';
import PermissionsMixin from 'fw-ember/mixins/permissions';

/**
 * @module fw-ember-gc
 */

/**
 * Route that includes several of the auth-related mixins already included,
 * made for convenience.
 *
 * Usage: (in a routes file)
 * ```javascript
 * import AuthRoute from './auth';
 *
 * export default AuthRoute.extend({
 *     // your code here...
 * });
 * ```
 *
 * @class AuthRoute
 * @extends BaseRoute
 * @uses AuthenticatedRouteMixin
 * @uses AuthCheckMixin
 * @uses PermissionsMixin
 */
export default BaseRoute.extend(AuthenticatedRouteMixin, AuthCheckMixin, PermissionsMixin);
