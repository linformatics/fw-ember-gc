import AdaptiveStore from 'ember-simple-auth/session-stores/adaptive';

// used by ember-simple-auth to store login session data
export default AdaptiveStore.extend({
    localStorageKey: 'fw:session',
    cookieName: 'fw:session'
});
