import BaseModal from './base';
import service from 'ember-service/inject';
import computed from 'ember-computed';
import {isEmberArray} from 'ember-array/utils';

export default BaseModal.extend({
    config: service(),

    developers: computed('config.developers', function () {
        let developers = this.get('config.developers');

        if (!developers) {
            return null;
        }

        return isEmberArray(developers) ? developers.join(', ') : developers;
    })
});
