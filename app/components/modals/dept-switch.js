import service from 'ember-service/inject';
import computed from 'ember-computed';
import BaseModal from './base';

export default BaseModal.extend({
    departments: null,

    dept: computed('departments', 'currentUser.currentDept', function () {
        if (this.get('departments')) {
            return this.get('departments').findBy('id', this.get('currentUser.currentDept'));
        }
    }),

    currentUser: service(),
    config: service(),
    ajax: service(),

    _loadDepartments() {
        return this.get('ajax').request(this.get('config').formUrl('depts', {program: this.get('config.programId'), nameOnly: 1, app: 'gc'}))
            .then(({depts}) => {
                this.set('departments', depts);
            });
    },

    didInsertElement() {
        this._super(...arguments);
        this._loadDepartments();
    },

    actions: {
        changeDepartment(dept) {
            if (!this.get('currentUser').checkDepartment(dept.id)) {
                this.get('currentUser').changeDepartment(dept.id).then(() => {
                    this.send('reload');
                }).catch(() => {
                    console.error('Failed to switch departments!');
                });
            }
        }
    }
});
