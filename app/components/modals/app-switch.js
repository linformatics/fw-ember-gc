import BaseModal from './base';
import service from 'ember-service/inject';
import {A as emberA} from 'ember-array/utils';

export default BaseModal.extend({
    apps: [],

    config: service(),
    currentUser: service(),
    ajax: service(),

    init() {
        this._super(...arguments);
        this._loadApps();
    },

    _loadApps() {
        let programs = this.get('currentUser.programs').join(',');
        let apps = [];

        return this.get('ajax').request(this.get('config').formUrl('apps', {ids: programs, app: 'gc'})).then((data) => {
            if (data && data.apps) {
                emberA(data.apps).forEach(function (prog) {
                    apps.push({
                        name: prog.name,
                        url: prog.url,
                        logo: `${prog.url.replace(/\/$/g, '')}/${prog.logo.replace(/^\//g, '')}`
                    });
                });
                this.set('apps', apps);
            }
        });
    }
});
