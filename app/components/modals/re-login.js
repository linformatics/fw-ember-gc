import service from 'ember-service/inject';
import BaseModal from './base';
import {
    validator, buildValidations
} from 'ember-cp-validations';
import ValidationMixin from 'fw-ember/mixins/validation';

const ReloginValidation = buildValidations({
    password: validator('password', {
        dependentKeys: ['wrongPassword']
    })
});

export default BaseModal.extend(ReloginValidation, ValidationMixin, {
    password: '',

    wrongPassword: true,

    config: service(),
    currentUser: service(),
    session: service(),
    ajax: service(),

    actions: {
        confirm() {
            let user = this.get('currentUser');
            let data = {
                user: user.get('userId'),
                pass: this.get('password'),
                program: this.get('config.programId')
            };
            let sessionUrl = this.get('config').formUrl('session', {app: 'gc'});

            this.set('wrongPassword', false);

            this.get('ajax').post(sessionUrl, {
                data
            }).then(() => {
                user.changeDepartment(user.get('currentDept'))
                    .then(() => {
                        let attemptedTransition = this.get('model.attemptedTransition');

                        this.set('password', '');
                        if (attemptedTransition) {
                            attemptedTransition.retry();
                        }
                        this.send('closeModal', false);
                    }).catch(() => {
                        this.send('closeModal', false);
                    });
            }).catch(() => {
                this.set('wrongPassword', true);
            });
        },

        closeModal(invalidate = true) {
            if (invalidate) {
                if (confirm('Are you sure you want to close? You will lose all of your unsaved data!')) {
                    this.get('session').invalidate();
                }
                return;
            }

            return this._super();
        }
    }
});
