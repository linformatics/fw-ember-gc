import Component from 'ember-component';
import on from 'ember-evented/on';
import {EKMixin, EKOnInsertMixin, keyUp} from 'ember-keyboard';
import {InvokeActionMixin} from 'ember-invoke-action';

export default Component.extend(EKMixin, EKOnInsertMixin, InvokeActionMixin, {
    tagName: 'article',
    classNames: 'fw-modal',

    confirm: on(keyUp('Enter'), function () {
        this.send('confirm');
    }),

    cancel: on(keyUp('Escape'), function () {
        this.send('closeModal');
    }),

    actions: {
        confirm() {},

        closeModal() {
            this.invokeAction('closeModal');
        }
    }
});
