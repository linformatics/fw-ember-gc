import service from 'ember-service/inject';
import {alias} from 'ember-computed';
import BaseModal from './base';
import {isBadRequestError} from 'ember-ajax/errors';
import {
    validator, buildValidations
} from 'ember-cp-validations';
import ValidationMixin from 'fw-ember/mixins/validation';

const ChangePasswordValidation = buildValidations({
    currentPass: [
        validator('presence', {
            presence: true,
            message: 'You must supply a current password.'
        }),
        validator('password', {
            dependentKeys: ['wrongPassword']
        })
    ],
    newPass: validator('presence', {
        presence: true,
        allowBlank: true,
        message: 'You must supply a new password.'
    }),
    confirmPass: validator('confirmation', {
        on: 'newPass',
        message: 'Passwords do not match.'
    })
});

export default BaseModal.extend(ChangePasswordValidation, ValidationMixin, {
    currentPass: '',
    newPass: '',
    confirmPass: '',

    wrongPassword: false,

    config: service(),
    ajax: service(),
    notifications: service(),

    needSecure: alias('model.needSecure'),

    actions: {
        confirm() {
            let data = this.getProperties('currentPass', 'newPass', 'confirmPass');
            let usersUrl = this.get('config').formUrl('users', 'changePassword', {app: 'gc'});

            this.set('wrongPassword', false);

            return this.get('ajax').put(usersUrl, {data});
        },

        success() {
            this.set('currentPass', '');
            this.set('newPass', '');
            this.set('confirmPass', '');
            this.clearValidations();

            this.get('notifications').showSuccess('Password changed successfully!', 'profile', true);

            this.send('closeModal');
        },

        error(error) {
            if (isBadRequestError(error)) {
                this.set('wrongPassword', true);
            }
        }
    }
});
