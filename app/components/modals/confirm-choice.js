import computed from 'ember-computed';
import {assign} from 'ember-platform';
import BaseModal from './base';

const buttonDefaults = {
    confirmButtonText: 'Confirm',
    confirmButtonStyle: 'primary',
    closeButtonText: 'Close'
};

export default BaseModal.extend({
    _model: computed('model', function () {
        return assign(buttonDefaults, this.get('model'));
    }),

    actions: {
        confirm() {
            if (this.get('model.onconfirm')) {
                return this.invokeAction('model.onconfirm').then(() => {
                    this.invokeAction('closeModal');
                });
            }
        }
    }
});
