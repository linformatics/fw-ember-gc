import PromiseButton from 'fw-ember/components/fw-promise-button';
import AuthComponentMixin from 'fw-ember/mixins/auth-component';

export default PromiseButton.extend(AuthComponentMixin);
