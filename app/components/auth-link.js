import AuthComponent from 'fw-ember/mixins/auth-component';
import Ember from 'ember';

const {LinkComponent} = Ember;

export default LinkComponent.extend(AuthComponent);
