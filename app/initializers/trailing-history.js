// credit to Ghost for this code
import HistoryLocation from 'ember-locations/history';

let trailingHistory = HistoryLocation.extend({
    formatURL() {
        return this._super(...arguments).replace(/\/?$/, '/');
    }
});

export default {
    name: 'registerTrailingHistoryLocation',

    initialize(application) {
        application.register('location:trailing-history', trailingHistory);
    }
};
