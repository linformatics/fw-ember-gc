import Ember from 'ember';
import Service from 'ember-service';
import computed from 'ember-computed';
import config from '../config/environment';

const {_ProxyMixin} = Ember;

/**
 * This service packages important configuration properties into a Service
 * that can be injected anywhere that is needed.
 *
 * Usage Example:
 *
 * *NOTE: this can only be used in an Ember object that has a container.
 * This list includes routes, components, controllers, other services, etc.*
 *
 * ```
 * import Ember from 'ember';
 *
 * const {
 *     Route,
 *     inject: {service}
 * } = Ember;
 *
 * export default Route.extend({
 *     config: service(), // config will be available as a property
 *
 *     // ... your other code
 * });
 *
 *
 * @class ConfigService
 * @extends Ember.Service
 * @module fw-ember
 */
export default Service.extend(_ProxyMixin, {

    /**
     * Computed property used for the ProxyMixin
     * Gets content from `config/environment.js`
     *
     * @property content
     * @type Object
     */
    content: computed(function () {
        let appConfig = config.APP.config;

        appConfig.url = config.baseURL;

        return appConfig;
    }),

    /**
     * Computed property that refers to the api rool url
     * Used to make api requests
     *
     * @property apiRoot
     * @type String
     */
    apiRoot: computed('api', 'url', function () {
        return this.get('url') + this.get('api');
    }),

    /**
     * Forms a url from the arguments specified.
     *
     * Expects arguments as strings, with the exception of the last
     * argument, which can be an object of properties to convert
     * to query parameters.
     *
     * Example usage:
     * ```
     * // assuming apiRoot is '/api/'
     * let id = 10
     * let url = config.formUrl('users', id, {include: 'posts'});
     *
     * // url is '/api/users/10/?include=posts'
     *
     * @method formUrl
     * @return {String}
     */
    formUrl() {
        let args = Array.prototype.slice.call(arguments);
        let queries = [];
        let key, url, queryObject;

        if (args[args.length - 1] instanceof Object) {
            queryObject = args.pop();
            for (key in queryObject) {
                if (queryObject.hasOwnProperty(key)) {
                    queries.push(`${key}=${encodeURIComponent(queryObject[key])}`);
                }
            }
        }

        url = `${this.get('apiRoot')}/${args.join('/')}/`;

        if (queries.length) {
            url += `?${queries.join('&')}`;
        }

        return url;
    }
});
