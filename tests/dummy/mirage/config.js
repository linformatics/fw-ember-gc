import env from 'dummy/config/environment';

let apiRoot = env.APP.config.url + env.APP.config.api;

export default function () {
    this.namespace = apiRoot;
}
