import EmberRouter from 'ember-router';
import config from './config/environment';
import authRouter from 'fw-ember/router';

const Router = EmberRouter.extend({
    location: config.locationType
});

Router.map(function () {
    authRouter(this);

    this.route('installation');
    this.route('basic-components', function () {
        this.route('content-box');
        this.route('notifications');
        this.route('tooltip-popover');
    });

    this.route('cookbook', function () {
        this.route('ember-power-select');
        this.route('ember-load');
    });
});

export default Router;
