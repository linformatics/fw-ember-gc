import Controller from 'ember-controller';
import service from 'ember-service/inject';

export default Controller.extend({
    notifications: service(),

    actions: {
        triggerGlobalNotification(message) {
            this.get('notifications').showSuccess(message, 'global', true);
        },

        triggerBasicNotification(message, outlet) {
            this.get('notifications').showWarning(message, outlet, true);
        }
    }
});
