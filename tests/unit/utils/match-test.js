/* jshint expr:true */
import { expect } from 'chai';
import {
    describe,
    it
} from 'mocha';
import match from 'fw-ember/utils/match';

describe('match', function () {
    it('correctly tests a single value against a single value', function () {
        expect(match('a', 'a')).to.be.true;
        expect(match('a', 'b')).to.be.false;
    });

    it('correctly tests a single value against a 1d array', function () {
        expect(match('a', ['a', 'a'])).to.be.true;
        expect(match('a', ['a', 'b'])).to.be.false;
        expect(match('a', ['a', 'a', 'a', 'b'])).to.be.false;
    });

    it('correctly tests a single value against a 2d array', function () {
        expect(match('a', ['a', ['a', 'b', 'c']])).to.be.true;
        expect(match('a', ['a', ['b', 'c', 'd']])).to.be.false;
        expect(match('a', ['a', ['a', 'b', 'c', 'd'], ['a', 'b', 'c'], 'a'])).to.be.true;
        expect(match('a', ['a', ['b', 'c', 'd'], ['a', 'd'], ['c']])).to.be.false;
    });
});
