/* jshint node:true */
/* jscs:disable */
module.exports = {
    normalizeEntityName: function () {},

    afterInstall: function () {
        var that = this;

        return this.addBowerPackageToProject('bootstrap-sass').then(function () {
            return that.addAddonsToProject({
                packages: [
                    {name: 'ember-truth-helpers', target: '1.2.0'},
                    {name: 'ember-simple-auth', target: '1.1.0'},
                    {name: 'liquid-tether', target: '0.1.11'},
                    {name: 'ember-keyboard', target: '1.1.0'},
                    {name: 'ember-route-action-helper', target: '0.3.0'},
                    {name: 'ember-cp-validations', target: '2.8.0'},
                    {name: 'ember-invoke-action', target: '1.3.0'},
                    {name: 'ember-one-way-controls', target: '0.8.0'}
                ]
            });
        });
    }
};
