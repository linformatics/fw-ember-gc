/* jscs:disable */
/* jshint node:true */
'use strict';

module.exports = {
    name: 'fw-ember',

    included: function (app) {
        this._super.included(app);

        this.importBootstrapJS(app);
    },

    importBootstrapJS: function (app) {
        var bootstrapJs = '/bootstrap-sass/assets/javascripts/bootstrap/';

        app.import(app.bowerDirectory + bootstrapJs + 'alert.js');
        app.import(app.bowerDirectory + bootstrapJs + 'dropdown.js');
        app.import(app.bowerDirectory + bootstrapJs + 'tooltip.js');
        app.import(app.bowerDirectory + bootstrapJs + 'popover.js');
    },
};
